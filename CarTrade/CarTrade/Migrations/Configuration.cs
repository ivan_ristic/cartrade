namespace CarTrade.Migrations
{
    using CarTrade.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CarTrade.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CarTrade.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            context.CarTrades.AddOrUpdate(x => x.Id,
                new CarTrades() { Id = 1, Name = "Auto Sporting", City = "Novi Sad", Adress = "Bulevar Oslobodjenja 55", Postcode = 21000, Email = "sporting@gmail.com", Phone = "0605006000" }
                );

            context.Cars.AddOrUpdate(x => x.Id,
                new Car()
                {
                    Id = 1,
                    Brand = "Alfa Romeo",
                    Model = "GT",
                    Year = 2005,
                    Capacity = 1991,
                    Power = 150,
                    Price = 2999,
                    Consumption = 6.1m,
                    VIN = "12345678911234567",
                    CarTradeId = 1

                },
                new Car()
                {
                    Id = 1,
                    Brand = "Alfa Romeo",
                    Model = "147",
                    Year = 2007,
                    Capacity = 1991,
                    Power = 120,
                    Price = 3600,
                    Consumption = 5.7m,
                    VIN = "15345678911234567",
                    CarTradeId = 1
                },
                new Car()
                {
                    Id = 1,
                    Brand = "BME",
                    Model = "320D",
                    Year = 2006,
                    Capacity = 1995,
                    Power = 163,
                    Price = 7200,
                    Consumption = 6.5m,
                    VIN = "55345678911234567",
                    CarTradeId = 1
                },
                new Car()
                {
                    Id = 1,
                    Brand = "BMW",
                    Model = "X5",
                    Year = 2007,
                    Capacity = 3000,
                    Power = 220,
                    Price = 8000,
                    Consumption = 9.7m,
                    VIN = "85345678911234567",
                    CarTradeId = 1
                },
                new Car()
                {
                    Id = 1,
                    Brand = "Audi",
                    Model = "A3",
                    Year = 2005,
                    Capacity = 1997,
                    Power = 140,
                    Price = 3800,
                    Consumption = 4.7m,
                    VIN = "15345678981234567",
                    CarTradeId = 1
                },
                new Car()
                {
                    Id = 1,
                    Brand = "Mazda",
                    Model = "3",
                    Year = 2007,
                    Capacity = 1598,
                    Power = 116,
                    Price = 3200,
                    Consumption = 6.7m,
                    VIN = "35345678911234567",
                    CarTradeId = 1
                },
                new Car()
                {
                    Id = 1,
                    Brand = "Ford",
                    Model = "Focus",
                    Year = 2004,
                    Capacity = 1598,
                    Power = 110,
                    Price = 2700,
                    Consumption = 4.7m,
                    VIN = "85345678931234567",
                    CarTradeId = 1
                },
                new Car()
                {
                    Id = 1,
                    Brand = "Citroen",
                    Model = "C5",
                    Year = 2006,
                    Capacity = 1999,
                    Power = 136,
                    Price = 3100,
                    Consumption = 6.3m,
                    VIN = "15345678951234567",
                    CarTradeId = 1
                },
                new Car()
                {
                    Id = 1,
                    Brand = "Peugeot",
                    Model = "308",
                    Year = 2007,
                    Capacity = 1600,
                    Power = 120,
                    Price = 4000,
                    Consumption = 5.7m,
                    VIN = "15345669911234567",
                    CarTradeId = 1
                },
                new Car()
                {
                    Id = 1,
                    Brand = "Subaru",
                    Model = "Impreza WRX",
                    Year = 2005,
                    Capacity = 2500,
                    Power = 300,
                    Price = 9000,
                    Consumption = 10.7m,
                    VIN = "55345678911234567",
                    CarTradeId = 1
                },
                new Car()
                {
                    Id = 1,
                    Brand = "Audi",
                    Model = "A4",
                    Year = 2007,
                    Capacity = 1991,
                    Power = 170,
                    Price = 5500,
                    Consumption = 8.7m,
                    VIN = "89345678911234567",
                    CarTradeId = 1
                },
                new Car()
                {
                    Id = 1,
                    Brand = "Fiat",
                    Model = "Punto",
                    Year = 2001,
                    Capacity = 1124,
                    Power = 60,
                    Price = 1100,
                    Consumption = 4.5m,
                    VIN = "15345678911234587",
                    CarTradeId = 1
                },
                new Car()
                {
                    Id = 1,
                    Brand = "Renault",
                    Model = "Megane II",
                    Year = 2007,
                    Capacity = 1999,
                    Power = 130,
                    Price = 2400,
                    Consumption = 5.57m,
                    VIN = "15345679971234567",
                    CarTradeId = 1
                },
                new Car()
                {
                    Id = 1,
                    Brand = "Alfa Romeo",
                    Model = "147",
                    Year = 2007,
                    Capacity = 1991,
                    Power = 120,
                    Price = 3600,
                    Consumption = 5.7m,
                    VIN = "15345678911234567",
                    CarTradeId = 1
                },
                new Car()
                {
                    Id = 1,
                    Brand = "Alfa Romeo",
                    Model = "156",
                    Year = 2000,
                    Capacity = 1747,
                    Power = 144,
                    Price = 2200,
                    Consumption = 8.1m,
                    VIN = "25345678911234567",
                    CarTradeId = 1
                });
        }
    }
}
