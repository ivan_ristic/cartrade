﻿using CarTrade.Interfaces;
using CarTrade.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CarTrade.Controllers
{
    public class CarController : ApiController
    {
        ICarRepository repo { get; set; }

        public CarController(ICarRepository repository)
        {
            repo = repository;
        }

        public IQueryable<Car> Get()
        {
            return repo.GetAll();
        }

        public IHttpActionResult Get(int id)
        {
            var car = repo.GetById(id);
            if (car == null)
            {
                return NotFound();
            }
            return Ok(car);
        }

        [Route("api/search")]
        public IQueryable<Car> GetFilter(int start, int kraj)
        {
            return repo.GetByCriteria(start, kraj);
        }

        [Route("api/power")]
        public IQueryable<Car> GetPower()
        {
            return repo.GetByPower();
        }

        [Route("api/price")]
        public IQueryable<Car> GetCheapest()
        {
            return repo.GetByPrice();
        }

        public IHttpActionResult Post(Car car)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            repo.Add(car);
            return CreatedAtRoute("DefaultApi", new { id = car.Id }, car);
        }

        public IHttpActionResult Put(int id, Car car)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != car.Id)
            {
                return BadRequest();
            }
            try
            {
                repo.Update(car);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(car);
        }
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var car = repo.GetById(id);
            if (car == null)
            {
                return NotFound();
            }
            repo.Delete(car);
            return Ok();
        }
    }
}
