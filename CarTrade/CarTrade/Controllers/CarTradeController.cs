﻿using CarTrade.Interfaces;
using CarTrade.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CarTrade.Controllers
{
    public class CarTradeController : ApiController
    {
        ICarTradeRepository repo { get; set; }

        public CarTradeController(ICarTradeRepository repository)
        {
            repo = repository;
        }

        public IQueryable<CarTrades> Get()
        {
            return repo.GetAll();
        }

        public IHttpActionResult Get(int id)
        {
            var carTrade = repo.GetById(id);
            if (carTrade == null)
            {
                return NotFound();
            }
            return Ok(carTrade);
        }

        //[Route("api/pretraga")]
        //public IQueryable<Car> GetFilter(int start, int kraj)
        //{
        //    return repo.GetByGames(start, kraj);
        //}

        public IHttpActionResult Post(CarTrades carTrade)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            repo.Add(carTrade);
            return CreatedAtRoute("DefaultApi", new { id = carTrade.Id }, carTrade);
        }

        public IHttpActionResult Put(int id, CarTrades carTrade)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != carTrade.Id)
            {
                return BadRequest();
            }
            try
            {
                repo.Update(carTrade);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(carTrade);
        }
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var carTrade = repo.GetById(id);
            if (carTrade == null)
            {
                return NotFound();
            }
            repo.Delete(carTrade);
            return Ok();
        }
    }
}
