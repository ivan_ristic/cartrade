﻿using CarTrade.Interfaces;
using CarTrade.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace CarTrade.Repository
{
    public class CarTradeRepository : IDisposable, ICarTradeRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public IQueryable<CarTrades> GetAll()
        {
            return db.CarTrades;
        }

        public CarTrades GetById(int id)
        {
            return db.CarTrades.FirstOrDefault(x => x.Id == id);
        }

        public void Add(CarTrades carTrade)
        {
            db.CarTrades.Add(carTrade);
            db.SaveChanges();
        }

        public void Update(CarTrades carTrade)
        {
            db.Entry(carTrade).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void Delete(CarTrades carTrade)
        {
            db.CarTrades.Remove(carTrade);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}