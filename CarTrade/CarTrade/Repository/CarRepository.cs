﻿using CarTrade.Interfaces;
using CarTrade.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace CarTrade.Repository
{
    public class CarRepository : IDisposable, ICarRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public IQueryable<Car> GetAll()
        {
            return db.Cars.Include(x => x.CarTrade);
        }

        public Car GetById(int id)
        {
            return db.Cars.FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<Car> GetByCriteria(int from, int to )
        {
            return db.Cars.Where(x => x.Year > from & x.Year < to);
        }

        public IQueryable<Car> GetByPrice()
        {
            return db.Cars.OrderBy(x=> x.Price).Take(5);
        }

        public IQueryable<Car> GetByPower()
        {
            return db.Cars.OrderByDescending(x => x.Power).Take(5);
        }

        public void Add(Car car)
        {
            db.Cars.Add(car);
            db.SaveChanges();
        }

        public void Update(Car car)
        {
            db.Entry(car).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void Delete(Car car)
        {
            db.Cars.Remove(car);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}