﻿$(document).ready(function () {
    // podaci od interesa
    var http = "http://";
    var host = window.location.host;
    var token = null;
    var headers = {};
    var carsEndPoint = "/api/car/";
    var carTradeEndPoint = "/api/cartrade/";
    var search = "/api/search";
    var price = "/api/price/";
    var power = "/api/power";

    var formAction = "Create";

    // pripremanje dogadjaja za brisanje
    $("body").on("click", "#btnDelete", deleteCar);
    // priprema dogadjaja za izmenu
    $("body").on("click", "#btnEdit", editCar);

   

    // posto inicijalno nismo prijavljeni, sakrivamo odjavu
    $("#logout").css("display", "none");

    //prikaz registracije kada se klikne na button
    $("#showReg").click(function (e) {
        e.preventDefault();
        $("#showLoginForm").css('display', 'none');
        $("#showRegForm").css('display', 'block');
    });

    // registracija korisnika
    $("#registration").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var pass = $("#regPass").val();
        var pass2 = $("#regPass2").val();

        // objekat koji se salje
        var sendData = {
            "Email": email,
            "Password": pass,
            "ConfirmPassword": pass2
        };
        $.ajax({
            type: "POST",
            url: http + host + "/api/Account/Register",
            data: sendData
        }).done(function (data) {
            $("#info").append("Successful registration, now you can log in to system.");
            refreshFormReg();
            $("#showLoginForm").css('display', 'block');
            $("#showRegForm").css('display', 'none');

        }).fail(function (data) {
            alert("Failed to register!");
        });
    });

    // prijava korisnika
    $("#login").submit(function (e) {
        e.preventDefault();

        var email = $("#logEmail").val();
        var pass = $("#logPass").val();

        // objekat koji se salje
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": pass
        };

        $.ajax({
            "type": "POST",
            "url": http + host + "/Token",
            "data": sendData

        }).done(function (data) {
            $("#info").empty().append("Log in user: " + data.userName);
            token = data.access_token;
            $("#showRegForm").css("display", "none");
            $("#showLoginForm").css("display", "none");
            $("#logout").css("display", "block");
            //ucitavam tabelu
            showCars();
        }).fail(function (data) {
            alert("Failed to log in");
        });
    });

    // odjava korisnika sa sistema
    $("#btnLogout").click(function () {
        token = null;
        headers = {};

        $("#showLoginForm").css("display", "block");
        $("#showRegForm").css("display", "none");
        $("#logout").css("display", "none");
        $("#info").empty();
        $("#message").empty();
        $("#seed").empty();
        $("#formCars").css("display", "none");
        $("#search").css("display", "none");
        refreshFormLog();
    });

    //Ajax poziv za GET products
    function showCars() {
        // korisnik mora biti ulogovan
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        $.ajax({
            "type": "GET",
            "url": http + host + carsEndPoint,
            "headers": headers
        }).done(function (data) {
            setCars(data);
        }).fail(function (data) {
            alert(data);
        });
    }

    function refreshFormReg() {
        // cistim formu
        $("#regEmail").val('');
        $("#regPass").val('');
        $("#regPass2").val('');
    }

    function refreshFormLog() {
        // cistim formu
        $("#logEmail").val('');
        $("#logPass").val('');

    }
    function setCarTrade(data) {
        console.log("Status: success");

        var $container = $("#seedTrade");
        $container.empty();

        console.log(data);
        // ispis naslova
        var div = $("<div></div>");
        var h1 = $("<h1>Trade</h1>");
        div.append(h1);
        // ispis tabele
        var table = $("<table class='table table-bordered table-striped table-hover'></table>");
        var tbody = $("<tbody></tbody>");
        var header = $("<tr><th>Name</th><th>City</th><th>Adress</th><th>Postcode</th><th>Phone</th><th>Email</th>");
        tbody.append(header);
        for (i = 0; i < data.length; i++) {
            // prikazujemo novi red u tabeli
            var row = "<tr>";
            // prikaz podataka
            var displayData = "<td>" + data[i].Name + "</td><td>" + data[i].City + "</td><td>" + data[i].Adress + "</td><td>" + data[i].Postcode + "</td><td>" + data[i].Phone + "</td><td>" + data[i].Email + "</td>";
            //// prikaz dugmadi za izmenu i brisanje
            //var stringId = data[i].Id.toString();
            //var displayDelete = "<td><button class='btn btn-danger btn-md' id=btnDelete name=" + stringId + ">Delete</button></td>";
            //var displayEdit = "<td><button class ='btn btn-success btn-md' id=btnEdit name=" + stringId + ">Edit</button></td>";
            row += displayData /* + displayDelete + displayEdit*/ + "</tr>";
            tbody.append(row);
            newId = data[i].Id;
        }

        table.append(tbody);
        div.append(table);
        div.append();

        // ispis novog sadrzaja
        $container.append(div);

    }
    function setCars(data) {
        console.log("Status: success");

        var $container = $("#seed");
        $container.empty();

        console.log(data);
        // ispis naslova
        var div = $("<div></div>");
        var h1 = $("<h1>Cars</h1>");
        div.append(h1);
        // ispis tabele
        var table = $("<table class='table table-bordered table-striped table-hover'></table>");
        var tbody = $("<tbody></tbody>");
        var header = $("<tr><th>Brand</th><th>Model</th><th>Year</th><th>Capacity</th><th>Power</th><th>Consumption</th><th>Price</th><th>VIN</th></tr>");
        tbody.append(header);
        for (i = 0; i < data.length; i++) {
            // prikazujemo novi red u tabeli
            var row = "<tr>";
            // prikaz podataka
            var displayData = "<td>" + data[i].Brand + "</td><td>" + data[i].Model + "</td><td>" + data[i].Year + "</td><td>" + data[i].Capacity + "</td><td>" + data[i].Power + "</td><td>" + data[i].Consumption + "</td><td>" + data[i].Price + "</td><td>" + data[i].VIN + "</td>";
            // prikaz dugmadi za izmenu i brisanje
            var stringId = data[i].Id.toString();
            var displayDelete = "<td><button class='btn btn-danger btn-md' id=btnDelete name=" + stringId + ">Delete</button></td>";
            var displayEdit = "<td><button class ='btn btn-success btn-md' id=btnEdit name=" + stringId + ">Edit</button></td>";
            row += displayData + displayDelete + displayEdit + "</tr>";
            tbody.append(row);
            newId = data[i].Id;
        }

        table.append(tbody);
        div.append(table);
        div.append();

        $("#formCars").css("display", "block");
        $("#search").css("display", "block");

        // ispis novog sadrzaja
        $container.append(div);

    }

    //function setReprezentacijaDropDown(selectId) {
    //    $(selectId).empty();
    //    var requestUrl = http + host + reprezentacijeEndPoint;
    //    $.getJSON(requestUrl, function (data, status) {
    //        $.each(data, function (i) {
    //            $(selectId).append($("<option></option>").val(data[i].Id).html(data[i].Naziv));
    //        });
    //    });
    //}

    //Ajax poziv za create unutar submita productForm-a
    $("#carForm").submit(function (e) {
        // sprecavanje default akcije forme
        e.preventDefault();

        var brand = $("#brand").val();
        var model = $("#model").val();
        var year = $("#year").val();
        var price = $("#price").val();
        var power = $("#power").val();
        var vin = $("#vin").val();
        var consumption = $("#consumption").val();
        var capacity = $("#capacity").val();

        //var ok = true;
        //if (!imeIPrezime) {
        //    alert("Ime je obavezno polje.");
        //    ok = false;
        //} else if (imeIPrezime.length < 2 || imeIPrezime.length > 100) {
        //    alert("Ime mora imati izmedju 2 i 100 karaktera.");
        //    ok = false;
        //} else if (!godinaRodjenja) {
        //    alert("Godina je obavezno polje.");
        //    ok = false;
        //} else if (godinaRodjenja < 1976 || godinaRodjenja > 1999) {
        //    alert("Godina mora biti pozitivan broj manji od 1999 a veci od 0");
        //    ok = false;
        //}
        //if (!ok) {
        //    return;
        //}

        var httpAction;
        var sendData;
        var url;

        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        // u zavisnosti od akcije pripremam objekat
        if (formAction === "Create") {
            httpAction = "POST";
            url = http + host + fudbaleriEndPoint;
            sendData = {
                "Brand": brand,
                "Model": model,
                "Year": year,
                "Power": power,
                "Price": price,
                "Consumption": consumption,
                "VIN": vin,
                "Capacity": capacity
            };
        }
        else {
            httpAction = "PUT";
            url = http + host + fudbaleriEndPoint + editingId.toString();
            sendData = {
                "Id": editingId,
                "Brand": brand,
                "Model": model,
                "Year": year,
                "Power": power,
                "Price": price,
                "Consumption": consumption,
                "VIN": vin,
                "Capacity": capacity
            };
        }

        console.log("Object for sending");
        console.log(sendData);

        $.ajax({
            "type": httpAction,
            "url": url,
            "headers": headers,
            "data": sendData
        })
            .done(function (data) {
                formAction = "Create";
                refreshCars();
            })
            .fail(function (data) {
                alert("Failed!");
            });
    });

    //Delete product
    function deleteCar() {
        // izvlacimo {id}
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        //Odnosi se na name html elementa koji je pozvao funkciju delete (ovu)
        var deleteID = this.name;
        // saljemo zahtev 
        $.ajax({
            "url": http + host + carsEndPoint + deleteID.toString(),
            "type": "DELETE",
            "headers": headers
        })
            .done(function (data) {
                refreshCars();
            })
            .fail(function (data) {
                alert("Failed to delete!");
            });
    }
    function editCar() {
        // izvlacimo id
        $("#formCars").css("display", "block");
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        //Odnosi se na name html elementa koji je pozvao funkciju edit
        var editId = this.name;
        // saljemo zahtev da dobavimo tog autora
        $.ajax({
            "url": http + host + carsEndPoint + editId.toString(),
            "type": "GET",
            "headers": headers
        })
            .done(function (data) {
                $("#brand").val(data.Brand);
                $("#model").val(data.Model);
                $("#year").val(data.Year);
                $("#power").val(data.Power);
                $("#price").val(data.Price);
                $("#capacity").val(data.Capacity);
                $("#vin").val(data.VIN);
                $("#consumption").val(data.Consumption);
                editingId = data.Id;
                formAction = "Update";
            })
            .fail(function (data) {
                formAction = "Create";
                alert("Failed to edit!");
            });
    }

    function refreshCars() {
        // cistim formu
        $("#brand").val('');
        $("#model").val('');
        $("#year").val('');
        $("#power").val('');
        $("#capacity").val('');
        $("#price").val('');
        $("#vin").val('');
        $("#consumption").val('');

        showCars();
    }

    function refreshFormFilter() {
        $("#start").val('');
        $("#kraj").val('');
    }

    $("#btnFilter").click(function () {
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        var kategorijaId = $("#kategorijaFilter").val();

        // saljemo zahtev na getproductsbycategory
        $.ajax({
            "url": http + host + "/api/proizvodi?kategorijaId=" + kategorijaId.toString(),
            "type": "GET",
            "headers": headers
        })
            .done(function (data) {
                setProizvod(data);
            })
            .fail(function (data) {
                alert("Desila se greska!");
            });
    });

    $('#btnReset').click(function (e) {
        e.preventDefault();
        refreshCars();
        refreshFormFilter();
    });

    $('#btnSearch').click(function (e) {
        e.preventDefault();
        var start = $("#start").val();
        var kraj = $("#kraj").val();

        var requestUrl = http + host + search + "?start=" + start.toString() + "&kraj=" + kraj.toString();
        console.log("URL zahteva: " + requestUrl);
        $.getJSON(requestUrl, setCars);
        refreshFormFilter();
    });

    $('#top5').click(function (e) {
        e.preventDefault();
       
        var requestUrl = http + host + price;
        console.log("URL zahteva: " + requestUrl);
        $.getJSON(requestUrl, setCars);
        refreshFormFilter();
    });

    $('#top5power').click(function (e) {
        e.preventDefault();

        var requestUrl = http + host + power;
        console.log("URL zahteva: " + requestUrl);
        $.getJSON(requestUrl, setCars);
        refreshFormFilter();
    });

    $('#detailsCarSalon').click(function (e) {
        e.preventDefault();

        var requestUrl = http + host + carTradeEndPoint;
        console.log("URL zahteva: " + requestUrl);
        $.getJSON(requestUrl, setCarTrade);
        refreshFormFilter();
    });

});