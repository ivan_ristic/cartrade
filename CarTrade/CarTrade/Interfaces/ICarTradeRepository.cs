﻿using CarTrade.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarTrade.Interfaces
{
    public interface ICarTradeRepository
    {
        IQueryable<CarTrades> GetAll();
        CarTrades GetById(int id);
        void Add(CarTrades carTrades);
        void Update(CarTrades carTrades);
        void Delete(CarTrades carTrades);
    }
}
