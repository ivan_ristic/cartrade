﻿using CarTrade.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarTrade.Interfaces
{
    public interface ICarRepository
    {
        IQueryable<Car> GetAll();
        Car GetById(int id);
        IQueryable<Car> GetByCriteria(int from, int to);
        IQueryable<Car> GetByPower();
        IQueryable<Car> GetByPrice();
        void Add(Car car);
        void Update(Car car);
        void Delete(Car car);
    }
}
