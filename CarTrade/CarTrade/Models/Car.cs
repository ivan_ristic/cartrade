﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarTrade.Models
{
    public class Car
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Brand { get; set; }
        [Required]
        [StringLength(50)]
        public string Model { get; set; }
        [Required]
        [StringLength(17, MinimumLength =17)]
        public string VIN { get; set; }
        [Required]
        [Range(1900, 2019)]
        public int Year { get; set; }
        [Required]
        [Range(1, 15000)]
        public int Capacity { get; set; }
        [Required]
        [Range(1, 5000)]
        public decimal Power { get; set; }
        [Required]
        [Range(1, 100)]
        public decimal Consumption { get; set; }
        [Required]
        [Range(1, double.MaxValue)]
        public decimal Price { get; set; }

        public int CarTradeId { get; set; }
        public CarTrades CarTrade { get; set; }
    }
}